`pspcod` should be 10 or 12.

pspxc = 1 ==> PZ

pspxc = 7 ==> PW

pspxc = 11 ==> PBE

pspxc = 18 ==> BLYP

pspxc = -101130 ==> LibXC's PBE

The coefficients k's (used in spin-coupling calculations for l /= 0)
must be specified.

